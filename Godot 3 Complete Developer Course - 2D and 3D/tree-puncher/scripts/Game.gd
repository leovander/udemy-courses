extends Node

export(PackedScene) var trunkScene

onready var firstTrunkPosition = $FirstTrunkPosition
onready var grave = $Grave
onready var timeLeft = $TimeLeft
onready var player = $Player
onready var timer = $Timer
onready var score = $Score

var lastSpawnPosition
var lastHasAxe = false
var lastAxeRight = false
var dead = false
var current_score = 0

var trunks = []

func _ready():
	lastSpawnPosition = firstTrunkPosition.position
	score.text = str(current_score)
	spawnFirstTrunks()
	
func _process(delta):
	if dead:
		return
	timeLeft.value -= delta
	if timeLeft.value <= 0:
		die()
	
func spawnFirstTrunks():
	for counter in range(5):
		var newTrunk = trunkScene.instance()
		add_child(newTrunk)
		newTrunk.position = lastSpawnPosition
		lastSpawnPosition.y -= newTrunk.spriteHeight
		newTrunk.initializeTrunk(false, false)
		trunks.append(newTrunk)

func addTrunk(axe, axeRight):
	var newTrunk = trunkScene.instance()
	add_child(newTrunk)
	newTrunk.position = lastSpawnPosition
	newTrunk.initializeTrunk(axe, axeRight)
	trunks.append(newTrunk)

func punchTree(fromRight):
	if !lastHasAxe:
		if rand_range(0, 100) > 50:
			lastAxeRight = rand_range(0, 100) > 50
			lastHasAxe = true
		else:
			lastHasAxe = false
	else:
		if rand_range(0, 100) > 50:
			lastHasAxe = true
		else:
			lastHasAxe = false
			
	current_score += 1
	score.text = str(current_score)
			
	addTrunk(lastHasAxe, lastAxeRight)
	
	trunks[0].remove(fromRight)
	trunks.pop_front()
	
	for trunk in trunks:
		trunk.position.y += trunk.spriteHeight
	
	timeLeft.value += 0.25
	if timeLeft.value > timeLeft.max_value:
		timeLeft.value = timeLeft.max_value
	
func die():
	grave.position.x = player.position.x
	player.queue_free()
	timer.start()
	grave.visible = true
	dead = true

func _on_Timer_timeout():
	get_tree().reload_current_scene()
