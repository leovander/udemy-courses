extends Node

onready var message = $Message
onready var btnCorrect = $Correct
onready var btnGreater = $Greater
onready var btnLess = $Less

# Guess between 0 - 1000
var guess
var min_guessed = 0
var max_guessed = 1000
var ended = false

func _ready():
	guess = (min_guessed + max_guessed) / 2
	message.text = guessString(guess)

func _process(delta):
	if Input.is_action_just_pressed("up"):
		tryGuess("up")
	elif Input.is_action_just_pressed("down"):
		tryGuess("down")
	elif Input.is_action_just_pressed("space"):
		if ended:
			restartGame()
		else:
			endGame()

# type up = greater or down = less
func tryGuess(type):
	if type == "up":
		min_guessed = guess
	else:
		max_guessed = guess
		
	guess = (min_guessed + max_guessed) / 2
	message.text = guessString(guess)

func endGame():
	ended = true;
	message.text = "Yes! I knew it! Your number was " + str(guess) + "!"
	btnCorrect.text = "Restart"
	btnLess.hide()
	btnGreater.hide()
	
func guessString(guess):
	return "Is " + str(guess) + " your number?"

func restartGame():
	get_tree().reload_current_scene()

func _on_Correct_pressed():
	if ended:
		restartGame()
	else:
		endGame()

func _on_Greater_pressed():
	tryGuess("up")

func _on_Less_pressed():
	tryGuess("down")
