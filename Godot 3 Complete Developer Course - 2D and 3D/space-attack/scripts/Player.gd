extends KinematicBody2D

const SPEED = 500

export(PackedScene) var projectile
export(PackedScene) var explosion

export var health = 100

onready var sprite = $Sprite
onready var timer = $Timer
onready var death_timer = $DeathTimer
onready var audio = $Audio
onready var explosion_audio = $ExplosionAudio

var half_player_width
var half_player_height
var can_shoot = true
var dead = false

signal player_damage

func _ready():
	half_player_width = (sprite.texture.get_width() * scale.x) / 2
	half_player_height = (sprite.texture.get_height() * scale.y) / 2
	
func _process(delta):
	if Input.is_action_pressed("left"):
		position.x -= SPEED * delta
	if Input.is_action_pressed("right"):
		position.x += SPEED * delta
	if Input.is_action_pressed("up"):
		position.y -= SPEED * delta
	if Input.is_action_pressed("down"):
		position.y += SPEED * delta
		
	if can_shoot and Input.is_action_pressed("shoot"):
		can_shoot = false
		var new_projectile = projectile.instance()
		get_parent().add_child(new_projectile)
		new_projectile.position = position
		timer.start()
#		audio.play()
		
	position.x = clamp(position.x, 0 + half_player_width, get_viewport_rect().size.x - half_player_width)
	
	position.y = clamp(position.y, (get_viewport_rect().size.y / 2) + half_player_height, get_viewport_rect().size.y - half_player_height);

func _on_Timer_timeout():
	can_shoot = true

func add_damage(damage):
	if dead:
		return
		
	health -= damage
	emit_signal("player_damage")
	
	if health <= 0:
		dead = true
		health = 0
		sprite.queue_free()
		var new_explosion = explosion.instance()
		new_explosion.position = global_position
		get_tree().get_root().add_child(new_explosion)
		new_explosion.get_node("explosion/animation").play("explosion")
#		explosion_audio.play()
		death_timer.start()
		set_process(false)

func _on_DeathTimer_timeout():
	get_tree().reload_current_scene()
