extends Area2D

export var speed = 200
export var damage = 10

var top_screen_limit

func _ready():
	connect("area_entered", self, "_on_area_entered")
	top_screen_limit = 0 - get_viewport_rect().size.y

func _process(delta):
	position.y -= speed * delta
	
	if position.y < top_screen_limit:
		queue_free()
		
func _on_area_entered(area):
	if area.is_in_group("Enemies"):
		area.add_damage(damage)
		queue_free()