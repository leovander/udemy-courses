extends Node

onready var health = $Health
onready var player = $Player
onready var score = $Score

var current_score = 0

func _ready():
	health.value = player.health
	health.max_value = player.health

func _on_Player_player_damage():
	health.value = player.health

func _on_Spawner_formation_defeated():
	current_score += 3
	score.text = str(current_score)