extends Area2D

export var speed = 450
export var damage = 10

var screen_limit

func _ready():
	connect("body_entered", self, "_on_area_entered")
	screen_limit = get_viewport_rect().size.y
	
func _process(delta):
	position.y += speed * delta
	if position.y >= screen_limit:
		queue_free()
	
func _on_area_entered(area):
	if area.name == "Player":
		area.add_damage(damage)
		queue_free()