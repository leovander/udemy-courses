extends Area2D

export(PackedScene) var projectile
export(PackedScene) var explode

onready var timer = $Timer
onready var collision = $CollisionPolygon2D
onready var shoot_audio = $ShootAudio
onready var explosion_audio = $ExplosionAudio
onready var enemy_sprite = $Sprite

export var speed = 50
export var health  = 30

var dead = false
var can_shoot = true

signal was_defeated

func _process(delta):
	if can_shoot:
		_shoot()
		
func _shoot():
	if dead:
		return
		
	can_shoot = false
	var new_projectile = projectile.instance()
	new_projectile.position = global_position
	get_tree().get_root().add_child(new_projectile)
	timer.wait_time = rand_range(0.25, 1.5)
	timer.start()
#	shoot_audio.play()

func add_damage(damage):
	health -= damage
	if health <= 0:
		dead = true
		collision.queue_free()
		enemy_sprite.hide()
		var new_explosion = explode.instance()
		new_explosion.position = global_position
		get_tree().get_root().add_child(new_explosion)
		new_explosion.get_node("explosion/animation").play("explosion")
		hide()
#		explosion_audio.play()
		emit_signal("was_defeated")

func _on_Timer_timeout():
	can_shoot = true

func _on_ExplosionAudio_finished():
#	emit_signal("was_defeated")
	pass
