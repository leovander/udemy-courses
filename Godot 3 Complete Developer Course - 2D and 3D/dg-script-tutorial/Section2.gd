extends Node

# Globals
var my_dogs_name = "Doug"
var my_friend_name = "Ana"
var my_name = "Israel"

var ages_array = [28, 19, 10]
var names_array = [my_name, my_friend_name, my_dogs_name]

func _ready():
	GDScriptAssignment()

# S2L8
func ifStatements():
	var my_dogs_age = 12
	var my_cats_age = 1
	
	if my_name == "Israel":
		print("Hello Israel")
	else:
		print("Hello Stranger")
	
	if my_dogs_age >= my_cats_age and my_name == "Israel":
		print("Israel's dog is older than the cat")
	else:
		print("The cat is older")
		
	if my_dogs_age >= 10 or my_cats_age >= 10:
		print("They are elders")
	elif my_dogs_age <= 1 or my_cats_age <= 1:
		print("They are babies")
	else:
		print("They are adults")

# S2L9
func ifOperators():
	var number_one = 10
	var number_two = 10
	var name_one = "Charles"
	var name_two = "Bob"
	
	if name_one == name_two:
		print("Names are equal!")
	elif name_one != name_two:
		print("Names are different!")
		
	if number_one == number_two:
		print("Numbers are equal!")
	elif number_one != number_two:
		print("Numbers are different!")
		
	if number_one > number_two:
		print("Number 1 is greater!")
	elif number_one < number_two:
		print("Number 2 is greater!")
		
	if number_one >= number_two:
		print("Number 1 is greater or equal!")
	elif number_one <= number_two:
		print("Number 2 is greater or equal!")

# S2L10
func noLoop():
	var counter = 0
	
	counter = counter + 1
	print(counter)
	counter += 1
	print(counter)
	counter += 1
	print(counter)
	counter += 1
	print(counter)
	counter += 1
	print(counter)

# S2L10
func whileLoop():
	var counter = 0
	
	while counter < 5:
		counter += 1
		print(counter)

# S2L10
func forLoop():
	for counter in range(1,6):
		print(counter)

# S2L11
func varScope():
	if true:
		var my_dogs_name = "Buub"
		print(my_dogs_name)
	
	print(my_dogs_name)
	
	var counter = "Floof"
	
	for counter in range(5):
		print(counter)
		
	print(counter)

# S2L12
func arrays():
	for current_name in names_array:
		print(current_name)
		
	for current_age in ages_array:
		print(current_age)
		
	for idx in range(0, names_array.size()):
		print(names_array[idx] + " - " + str(ages_array[idx]))

# S2L13
func arrayFuncs():
	names_array.append("Frank")
	print(names_array[names_array.size() - 1])
	
	names_array.pop_back()
	print(names_array[names_array.size() - 1])
	
	print("First name before pop: " + names_array[0])
	names_array.pop_front()
	print("First name after pop: " + names_array[0])

# S2L14
func functions():
	sayMyName()
	howMuchIs9Plus10()
	multiplyBy9(5)
	var score = multiply(10,5)
	print(nameAge(names_array[0], ages_array[0]))

# S2L14
func sayMyName():
	print(my_name)

# S2L14
func howMuchIs9Plus10():
	print(9 + 10)

# S2L14
func multiplyBy9(num):
	var result = num * 9
	
	if result > 100:
		print("Number too big")
	else:
		print(result)

# S2L14
func multiply(num, multiplier):
	return num * multiplier

# S2L14
func nameAge(name, age):
	return name + " - " + str(age)

# Assignment 1
func GDScriptAssignment():
	# Create an array with 8 or more items for shopping
	var shoppingItems = ["Apple", "Banana", "Carrot", "Dumpling", "Eggplant", "Fig", "Garlic", "Honeydew Melon"]
	
	# Remove the first and last item of the array
	shoppingItems.pop_front()
	shoppingItems.pop_back()
	
	for shoppingItem in shoppingItems:
		print(displayShoppingItem(shoppingItem))

# Assignment 1
# Create a function that recieves an item as a parameter and returns a string as such:
# [Item Name] -> Buy
func displayShoppingItem(itemName):
	return "[" + itemName + "] -> Buy"